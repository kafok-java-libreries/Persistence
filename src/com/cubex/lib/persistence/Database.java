package com.cubex.lib.persistence;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public abstract class Database {
	
	protected SessionFactory sf;
	
	protected void createConfig(Map<String, String> configs, String driver, String url, String user, String pass, String dialect, String auto, String... packageScann) {
		if(configs == null)
			configs = new HashMap<String, String>();
		configs.put("hibernate.connection.driver_class", driver);
		configs.put("hibernate.connection.url", url);
		configs.put("hibernate.connection.user", user);
		configs.put("hibernate.connection.password", pass);
		configs.put("hibernate.dialect", dialect);
		configs.put("hibernate.hbm2ddl.auto", auto);
		
		createConfig(configs, packageScann);
	}
	
	protected void createConfig(Map<String, String> configs, String... packageScann) {
		Configuration cfg = new Configuration();
		cfg.setProperty("hibernate.format_sql", "true");
		cfg.setProperty("hibernate.show_sql", "false");
		cfg.setProperty("hibernate.cglib.use_reflection_optimizer", "true");
		cfg.setProperty("hibernate.generate_statistics", "false");
		for(Map.Entry<String, String> prop : configs.entrySet())
			cfg.setProperty(prop.getKey(), prop.getValue());
		
		for(String p : packageScann)
			for(Class<? extends Object> clazz : Reflexion.getAnnotedClasses(p, Entity.class))
				cfg.addAnnotatedClass(clazz);
		
		sf = cfg.buildSessionFactory();
	}
		
	public Session openSession() {
		return sf.openSession();
	}
	
	
	public void close() {
		sf.close();
	}
}
