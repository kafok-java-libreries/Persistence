package com.cubex.lib.persistence;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

@SuppressWarnings("unchecked")
public abstract class Repository {
	
	
	public static <T> T save(Session s, T entity) {
		T res = (T) s.merge(entity);
		s.save(res);
		return res;
	}

	public static <T, ID> T findOne(Session s, Class<T> type, ID id) {
		return s.find(type, id);
	}
	
	public static <T> List<T> findAll(Session s, Class<T> type) {
		Query<T> qry = s.createQuery("select t from " + type.getSimpleName() + " t");
		return qry.getResultList();
	}
	
	public static <T> List<T> findAll(Session s, Class<T> type, int page, int pageSize) {
		Query<T> qry = s.createQuery("select t from " + type.getSimpleName() + " t");
		qry.setFirstResult((page - 1)*pageSize);
		qry.setMaxResults(pageSize);
		return qry.getResultList();
	}
	
	public static void executeQuery(Session s, String query) {
		Query<?> qry = s.createQuery(query);
		qry.executeUpdate();
	}

	public static <T> List<T> query(Session s, String query) {
		Query<T> qry = s.createQuery(query);
		return qry.getResultList();
	}
	
	public static <T> T singleQuery(Session s, String query) {
		Query<T> qry = s.createQuery(query);
		return qry.getSingleResult();
	}
	
	public static <T> List<T> query(Session s, String query, int page, int pageSize) {
		Query<T> qry = s.createQuery(query);
		qry.setFirstResult((page - 1)*pageSize);
		qry.setMaxResults(pageSize);
		return qry.getResultList();
	}
	
	public static <T, ID, C> List<C> getCollection(Session s, ID id, Class<C> clazz) {
		Query<C> qry = s.createQuery("select t Form " + clazz.getSimpleName() + " t where t.id = " + id);
		return qry.getResultList();
	}
	
	public static <ID, C> List<C> getCollection(Session s, ID id, Class<C> clazz, int page, int pageSize) {
		Query<C> qry = s.createQuery("select t Form " + clazz.getSimpleName() + " t where t.id = " + id);
		qry.setFirstResult((page - 1)*pageSize);
		qry.setMaxResults(pageSize);
		return qry.getResultList();
	}
	
	public static <ID, C> List<C> getCollection(Session s, ID id, Class<C> clazz, String idAttribute) {
		Query<C> qry = s.createQuery("select t Form " + clazz.getSimpleName() + " t where t." + idAttribute + " = " + id);
		return qry.getResultList();
	}
	
	public static <ID, C> List<C> getCollection(Session s, ID id, Class<C> clazz, String idAttribute, int page, int pageSize) {
		Query<C> qry = s.createQuery("select t Form " + clazz.getSimpleName() + " t where t." + idAttribute + " = " + id);
		qry.setFirstResult((page - 1)*pageSize);
		qry.setMaxResults(pageSize);
		return qry.getResultList();
	}
}
