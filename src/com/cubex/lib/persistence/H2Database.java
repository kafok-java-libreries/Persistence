package com.cubex.lib.persistence;

import java.io.File;
import java.util.Map;

public class H2Database extends Database {
	
	private static final String DRIVER = "org.h2.Driver";
	private static final String DIALECT = "org.hibernate.dialect.H2Dialect";
	
	public H2Database(String path, String user, String pass, Map<String, String> configs, String... packageName) {
		
		//calculamos url
		path = new File(path).getAbsolutePath();
		String url = "jdbc:h2:file:" + path + ";MV_STORE=FALSE;TRACE_LEVEL_FILE=0;FILE_LOCK=FS";
		
		//calculamos auto
		String auto = (new File(path + ".h2.db")).exists() ? "validate" : "create";
		
		createConfig(configs, DRIVER, url, user, pass, DIALECT, auto, packageName);
	}
	
	public H2Database(String path, Map<String, String> configs, String... packageName) {
		
		//calculamos url
		path = new File(path).getAbsolutePath();
		String url = "jdbc:h2:file:" + path + ";MV_STORE=FALSE;TRACE_LEVEL_FILE=0;FILE_LOCK=FS";
		
		//calculamos auto
		String auto = (new File(path + ".h2.db")).exists() ? "validate" : "create";
		
		createConfig(configs, DRIVER, url, "", "", DIALECT, auto, packageName);
	}
}
