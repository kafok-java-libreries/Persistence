package com.cubex.lib.persistence;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Reflexion {
	
	private static void checkDirectory(File directory, String pckgname, List<Class<?>> classes) throws ClassNotFoundException {
		File tmpDirectory;

		if(directory.exists() && directory.isDirectory()) {
			String[] files = directory.list();

			for(final String file : files) {
				if(file.endsWith(".class")) {
					try {
						Class<?> clazz = Class.forName(pckgname + '.' + file.substring(0, file.length() - 6));
						classes.add(clazz);
					} catch (final NoClassDefFoundError e) {}
				} else if((tmpDirectory = new File(directory, file)).isDirectory())
					checkDirectory(tmpDirectory, pckgname + "." + file, classes);
			}
		}
	}
	
	private static void checkJarFile(JarURLConnection connection, String pckgname, List<Class<?>> classes) 
			throws ClassNotFoundException, IOException {
		JarFile jarFile = connection.getJarFile();
		Enumeration<JarEntry> entries = jarFile.entries();
		String name;

		for(JarEntry jarEntry = null; entries.hasMoreElements() && ((jarEntry = entries.nextElement()) != null);) {
			name = jarEntry.getName();

			if(name.contains(".class")) {
				name = name.substring(0, name.length() - 6).replace('/', '.');

				if(name.contains(pckgname)) {
					Class<?> clazz = Class.forName(name);
					classes.add(clazz);
				}
			}
		}
	}
	
	public static List<Class<?>> getAnnotedClasses(String pckgname, Class<? extends Annotation> annotation) {
		List<Class<?>> classes = new LinkedList<Class<?>>();

		for(Class<?> c : getClasses(pckgname))
			if(c.isAnnotationPresent(annotation))
				classes.add(c);

		return classes;
	}
	
	public static List<Class<?>> getClasses(String pckgname) {
		List<Class<?>> classes = new LinkedList<Class<?>>();

		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

			Enumeration<URL> resources = classLoader.getResources(pckgname.replace('.', '/'));
			URLConnection connection;

			for(URL url = null; resources.hasMoreElements() && ((url = resources.nextElement()) != null);) {
				connection = url.openConnection();

				if (connection instanceof JarURLConnection)
					checkJarFile((JarURLConnection) connection, pckgname, classes);
				else
					checkDirectory(new File(URLDecoder.decode(url.getPath(), "UTF-8")), pckgname, classes);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return classes;
	}
	
}
